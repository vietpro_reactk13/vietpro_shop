import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from "react-redux"

import {api, PRODUCT_API} from '../config/api';

export class Navbar extends Component {
    constructor(props, context) {
        super(props, context);
        
        this.state = {
            category: [],
        }
    }
    
    getCategories() {
        api.get(PRODUCT_API + '/category')
            .then(response => {
                // console.log("Data:", response.data)
                this.setState({
                    ...this.state,
                    category: response.data
                })
            })
            .catch(err => {
                console.log(err)
            })
        // console.log("get category")
    }
    
    componentDidMount() {
        this.getCategories()
    }
    
    render() {
        return (
            <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                    <nav>
                        <div id="menu" className="collapse navbar-collapse">
                            <ul>
                                {this.state.category.map((item, index) => 
                                    <li className="menu-item" key={index}>
                                        <Link to={"/category/" + item._id}>{item.name}</Link>
                                    </li>
                                )}
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        )
  }
}

const mapStateToProps = state => {
    return {
        // cart: state.cart.listProduct
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // dispatch1: () => {
        //     dispatch()
        // }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)