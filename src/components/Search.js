import React, { Component } from 'react'
import { connect } from "react-redux";
import { api, PRODUCT_API } from '../config/api';

export class Search extends Component {

    constructor(props, context) {
        super(props, context)
        
        this.state = {
            productList: []
        }
    }
    

    getProduct() {
        let search = this.props.location.search.slice(3)
        api.get(PRODUCT_API + '/search?q=' + search)
            .then(response => {
                console.log(response.data)
            })
    }

    render() {
        this.getProduct()
        return (
            <div className="products">
                <div id="search-result">
                    Kết quả tìm kiếm với sản phẩm <span>iPhone Xs Max 2 Sim - 256GB</span>
                </div>

                {/* {this.state.productList.map(item => <>)} */}

                <div className="product-list card-deck">
                    <div className="product-item card text-center">
                        <a href="/"><img src="images/product-1.png" alt=""/></a>
                        <h4><a href="/">iPhone Xs Max 2 Sim - 256GB</a></h4>
                        <p>Giá Bán: <span>32.990.000đ</span></p>
                    </div>
                    <div className="product-item card text-center">
                        <a href="/"><img src="images/product-2.png" alt=""/></a>
                        <h4><a href="/">iPhone Xs Max 2 Sim - 256GB</a></h4>
                        <p>Giá Bán: <span>32.990.000đ</span></p>
                    </div>
                    <div className="product-item card text-center">
                        <a href="/"><img src="images/product-3.png" alt=""/></a>
                        <h4><a href="/">iPhone Xs Max 2 Sim - 256GB</a></h4>
                        <p>Giá Bán: <span>32.990.000đ</span></p>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // prop: state.prop
    }
}

export default connect(mapStateToProps)(Search)