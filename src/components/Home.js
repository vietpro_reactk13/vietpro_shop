import React, { Component } from 'react'
import ProductItem from './ProductItem';
import {api, PRODUCT_API} from '../config/api';

export default class Home extends Component {
    constructor(props, context) {
        super(props, context);
        
        this.state = {
            dataFeturedProduct: [{
                        name: "iPhone Xs Max 2 Sim - 256GB",
                        image: "images/product-2.png",
                        price: 10000000,
                    },
                ],
            dataNewProduct: [
                // {
                //     name: "iPhone Xs Max 2 Sim - 256GB",
                //     image: "images/product-6.png",
                //     price: 10000000,
                // },
                // {
                //     name: "iPhone Xs Max 2 Sim - 256GB",
                //     image: "images/product-2.png",
                //     price: 10000000,
                // },
                // {
                //     name: "iPhone Xs Max 2 Sim - 256GB",
                //     image: "images/product-6.png",
                //     price: 20000000,
                // },
                // {
                //     name: "iPhone Xs Max 2 Sim - 256GB",
                //     image: "images/product-8.png",
                //     price: 10000000,
                // },
                // {
                //     name: "iPhone Xs Max 2 Sim - 256GB",
                //     image: "images/product-2.png",
                //     price: 10000000,
                // },
                // {
                //     name: "iPhone Xs Max 2 Sim - 256GB",
                //     image: "images/product-3.png",
                //     price: 20000000,
                // },
            ]
        }

        this.changeData = this.changeData.bind(this)
    }

    // renderData() {
    //     let result = []

    //     for (let i = 0; i < this.data.length; i++) {
    //         let element =  <ProductItem 
    //                             name={this.data[i].name} 
    //                             image={this.data[i].image}
    //                             price={this.data[i].price}
    //                         />
    //         result.push(element)
    //     }

    //     return result
    // }

    changeData(data) {

        // console.log("This is a function of Home Component")
        // this.state.dataFeturedProduct.push({
        //     name: "iPhone Xs Max 2 Sim - 256GB",
        //     image: "images/product-5.png",
        //     price: 20000000,
        // })

        this.setState({
            ...this.state,
            dataFeturedProduct: [...this.state.dataFeturedProduct, data]
        })

        // console.log(this.dataFeturedProduct)
    }

    // shouldComponentUpdate(...args) {
    //     // console.log(...args)
    //     console.log("should component update")

    //     return true
    // }

    // componentDidUpdate() {
    //     console.log("component did update")
    // }

    getFeaturedProducts() {
        api.get(PRODUCT_API + '/featured')
            .then(response => {
                this.setState({
                    ...this.state,
                    dataFeturedProduct: response.data.slice(0, 6)
                })
            })
            .catch(error => console.log(error))
    }
    
    componentDidMount() {
        this.getFeaturedProducts()
        // console.log("component did mount")
    }

    render() {
        // console.log("render")
        return (
            <div>
                {/* Feature Product */}
                <div className="products">
                    <h3>Sản phẩm nổi bật</h3>
                    <div className="product-list card-deck">

                        {this.state.dataFeturedProduct.map((item, index) => (
                            <ProductItem
                                _id={item._id}
                                name={item.name}
                                image={item.image}
                                price={item.price}
                                changeData={this.changeData}
                                key={index}
                            />
                        ))}
                        
                    </div>
                </div>
                {/* End Feature Product */}
                {/* Latest Product */}
                <div className="products">
                    <h3>Sản phẩm mới</h3>
                    <div className="product-list card-deck">
                        {this.state.dataNewProduct.map((item, index) => (
                            <ProductItem
                                name={item.name}
                                image={item.image}
                                price={item.price}
                                key={index}
                            />
                        ))}
                    </div>
                </div>
                {/* End Latest Product */}
            </div>
        )
    }
}