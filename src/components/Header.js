import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

export class Header extends Component {

    constructor(props, context) {
        super(props, context)
        
        this.state = {
            search: "",
        }
    }

    handleInput = (event) => {
        let search = event.target.value

        this.setState({
            ...this.state,
            search,
        })

        if (event.keyCode === 13) {
            this.props.history.push('/search?q=' + search)
        }
    }

    render() {
        return (
            <div id="header">
                <div className="container">
                    <div className="row">
                        <div id="logo" className="col-lg-3 col-md-3 col-sm-12">
                            <h1><Link to="/"><img className="img-fluid" src="/images/logo.png" alt="" /></Link></h1>
                        </div>
                        <div id="search" className="col-lg-6 col-md-6 col-sm-12">
                            <div className="form-inline">
                                <input onKeyUp={this.handleInput} className="form-control mt-3" type="search" placeholder="Tìm kiếm" />
                                <Link to={"/search?q=" + this.state.search} className="btn btn-danger mt-3">Tìm kiếm</Link>
                            </div>
                        </div>
                        <div id="cart" className="col-lg-3 col-md-3 col-sm-12">
                            <Link className="mt-4 mr-2" to="/cart">giỏ hàng</Link><span className="mt-3">{this.props.cart.length}</span>
                        </div>
                    </div>
                </div>
                <button className="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#menu">
                    <span className="navbar-toggler-icon"></span>
                </button>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        cart: state.cart.listProduct,
        // search: state.general.search
    }
}

export default connect(mapStateToProps)(Header)