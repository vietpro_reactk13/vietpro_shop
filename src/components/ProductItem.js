import React, { Component } from 'react'
import { Link } from 'react-router-dom';


import { IMAGE_PRODUCT_BASE } from '../config/api';

export default class ProductItem extends Component {
    constructor(props, context) {
        super(props, context);
        
        this.addProduct = this.addProduct.bind(this)
    }

    formatMoney(n, c = ",") {
        return Math.floor(n).toString().replace(/\B(?=(\d{3})+(?!\d))/g, c);
    }

    addProduct(e) {
        this.props.changeData({
            name: "iPhone Xs Max 2 Sim - 512GB",
            image: "images/product-4.png",
            price: 10000000,
        })
    }

    deleteProduct(productIndex) {
        this.props.deleteProdcut(productIndex)
    }

    render() {
        // console.log(this.props)
        return (
            <div className="product-item card text-center">
                <Link to={"/product/" + this.props._id}>
                    <img alt="" src={IMAGE_PRODUCT_BASE + this.props.image} onClick={this.addProduct}/>
                </Link>
                <h4>
                    <Link to={"/product/" + this.props._id}>{this.props.name}</Link>
                </h4>
                <p>Giá Bán: <span>{this.formatMoney(this.props.price, ".")}đ</span></p>
            </div>
        )
    }
}