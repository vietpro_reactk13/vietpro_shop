import React, { Component } from 'react'
import { connect } from "react-redux"

import {api, PRODUCT_API} from '../config/api';
import ProductItem from './ProductItem';

// import store from '../redux/store'

class Category extends Component {
    constructor(props, context) {
        super(props, context);
        
        this.state = {
            isLoading: true,
        }

        // store.subscribe(() => {
        //     let state = store.getState()
        //     this.setState({
        //         ...this.state,
        //         listProduct: state.listProduct
        //     })
        // })
    }
    
    getFeaturedProducts() {
        let categoryId = this.props.match.params.categoryId
        this.setState({
            ...this.state,
            isLoading: true,
        })

        api.get(PRODUCT_API + '/category/' + categoryId)
            .then(res => {

                // this.setState({
                //     ...this.state,
                //     listProduct: res.data,
                //     isLoading: false,
                // })
                console.log(res.data)
                this.props.setListProduct(res.data)
                // store.dispatch({type: "SET_PRODUCT_LIST", payload: res.data})

                this.setState({
                    ...this.state,
                    isLoading: false
                })
            })
    }

    // shouldComponentUpdate(nextProps) {
    //     // if (this.props.match)

    //     return true
    // }

    // componentWillUpdate() {

    // }

    componentDidUpdate(preProps) {
        console.log("component did update")

        if (this.props.match.params.categoryId !== preProps.match.params.categoryId) {
            this.getFeaturedProducts()
        }
    }

    componentDidMount() {
        console.log("component did mount")
        this.getFeaturedProducts()
    }
    
    render() {
        return (
            <div className="products">
                {this.state.isLoading ? <Loading /> : 
                    <>
                        <h3>iPhone (hiện có 186 sản phẩm)</h3>
                        <div className="product-list card-deck">
                            {this.props.productList.map((item, index) => 
                                <ProductItem 
                                    name={item.name}
                                    image={item.image}
                                    price={item.price}
                                    key={index}
                                />
                            )}
                        </div>
                    </>
                }
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        productList: state.listProduct
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setListProduct: (listProduct) => {
            dispatch({ type: "SET_PRODUCT_LIST", payload: listProduct })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Category)

function Loading() {
    return (
        <div>Loading ...</div>
    )
}