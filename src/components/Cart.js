import React, { Component } from 'react'
import { connect } from "react-redux"
import { api, PRODUCT_API, IMAGE_PRODUCT_BASE } from '../config/api';
import { formatMoney } from '../lib/utils';

class Cart extends Component {

    constructor(props, context) {
        super(props, context)
        
        this.state = {
            listProduct: [],
            productAmount: [],
            formData: {
                name: '',
                phone: '',
                email: '',
                address: '',
            }
        }
    }
    
    async getProduct(productId) {
        let response = await api.get(PRODUCT_API + "/details/" + productId)
        return response.data
    }

    async componentDidMount() {
        const { listProduct } = this.props

        // Cách 1: (không nên dùng do performance thấp)
        // let listProductData = []
        // for (let i = 0; i < listProduct.length; i++) {
        //     let productData = await this.getProduct(listProduct[i])
        //     listProductData.push(productData)
        // }

        // Cách 2: nên dùng
        let listProductData = await Promise.all(listProduct.map(id => this.getProduct(id)))

        // Cách 3: cách ngày xưa khi dùng nhiều promise liên tiếp
        // let listProductData = []
        // for (let i = 0; i < listProduct.length; i++) {
        //     this.getProduct(listProduct[i])
        //         .then(response => {
        //             listProductData.push(response.data)
                    
        //             if (listProductData.length === listProduct.length) {
        //                 console.log("prd data", listProductData)
        //             }
        //         })
        // }
        
        // console.log(listProductData)

        this.setState({
            ...this.state,
            listProduct: listProductData,
            productAmount: new Array(listProductData.length).fill(1)
        })
    }

    calculateTotalBills() {
        let total = 0
        for (let i = 0, length = this.state.listProduct.length; i < length; i++) {
            let unitBill = this.state.listProduct[i].price * this.state.productAmount[i]
            total += unitBill
        }
        return total

        // return this.state.listProduct.reduce((total, item, index) => {
        //     return total + item.price * this.state.productAmount[index]
        // }, 0)

    }

    handleInputChange = (index, value) => {
        // Copy lại array để tránh mutate state
        let productAmount = [...this.state.productAmount]
        productAmount[index] = value

        this.setState({
            ...this.state,
            productAmount: productAmount
        })
    }

    // Xử lí multiple form trong React
    handleFormChange = (event) => {
        let { name, value } = event.target 

        this.setState({
            ...this.state,
            formData: {
                ...this.state.formData,
                [name]: value
            }
        })
    }

    submitForm = () => {

        const { name, phone, email, address } = this.state.formData

        let err = []

        if (!name) {
            err.push('Name is not valid')
        }
        if (!phone) {
            err.push('Phone is not valid')
        }
        
        if (err.length > 0) {
            for (let i = 0; i < err.length; i++) {
                alert(err[i])
            }
            return
        }

        let data = {
            name,
            phone,
            email,
            address,
            orders: this.state.listProduct
        }

        api.post(PRODUCT_API + '/order', data).then(response => {
            console.log(response.data)
            if (response.data.status === "oke") {
                this.props.history.push('/')
            }
        })
    }

    render() {
        // console.log(this.calculateTotalBills())
        // console.log(this.state)
        return (
            <div>
                <div id="my-cart">
                	<div className="row">
                        <div className="cart-nav-item col-lg-7 col-md-7 col-sm-12">Thông tin sản phẩm</div> 
                        <div className="cart-nav-item col-lg-2 col-md-2 col-sm-12">Tùy chọn</div> 
                        <div className="cart-nav-item col-lg-3 col-md-3 col-sm-12">Giá</div>    
                    </div>  
                    <form method="post">

                        {this.state.listProduct.map((productData, index) => (
                            <CartItem 
                                key={productData._id}
                                index={index}
                                productData={productData}
                                handleInputChange={this.handleInputChange}
                            />  
                        ))}
                        
                        <div className="row">
                            <div className="cart-thumb col-lg-7 col-md-7 col-sm-12"></div> 
                            <div className="cart-total col-lg-2 col-md-2 col-sm-12"><b>Tổng cộng:</b></div> 
                            <div className="cart-price col-lg-3 col-md-3 col-sm-12"><b>{formatMoney(this.calculateTotalBills())}đ</b></div>
                        </div>
                    </form>
                               
                </div>

                <div id="customer">
                	<form method="post">
                        <div className="row">
                            
                            <div id="customer-name" className="col-lg-4 col-md-4 col-sm-12">
                                <input placeholder="Họ và tên (bắt buộc)" type="text" name="name" className="form-control" required onChange={this.handleFormChange} />
                            </div>
                            <div id="customer-phone" className="col-lg-4 col-md-4 col-sm-12">
                                <input placeholder="Số điện thoại (bắt buộc)" type="text" name="phone" className="form-control" required onChange={this.handleFormChange} />
                            </div>
                            <div id="customer-mail" className="col-lg-4 col-md-4 col-sm-12">
                                <input placeholder="Email (bắt buộc)" type="text" name="email" className="form-control" required onChange={this.handleFormChange} />
                            </div>
                            <div id="customer-add" className="col-lg-12 col-md-12 col-sm-12">
                                <input placeholder="Địa chỉ nhà riêng hoặc cơ quan (bắt buộc)" type="text" name="address" className="form-control" required onChange={this.handleFormChange} />
                            </div>
                            
                        </div>
                    </form>
                    <div className="row">
                    	<div className="by-now col-lg-6 col-md-6 col-sm-12">
                        	<button onClick={this.submitForm}>
                            	<b>Mua ngay</b>
                                <span>Giao hàng tận nơi siêu tốc</span>
                            </button>
                        </div>
                        <div className="by-now col-lg-6 col-md-6 col-sm-12">
                        	<button>
                            	<b>Trả góp Online</b>
                                <span>Vui lòng call (+84) 0988 550 553</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// const postRequest = {
//     "name": "Nguyen van a",
//     "phone": "090129302193",
//     "email": "duyk16@gmail.com",
//     "address": "asdkasldksadkasdl 12 312 3adas dksld kals dklask d",
//     "orders": ["123123", 1230123, "asdasd12"]
// }

const CartItem = (props) => {
    return (
        <div className="cart-item row" key={props.productData._id}>
            <div className="cart-thumb col-lg-7 col-md-7 col-sm-12">
                <img alt="" src={IMAGE_PRODUCT_BASE + props.productData.image} />
                <h4>{props.productData.name}</h4>
            </div> 
            
            <div className="cart-quantity col-lg-2 col-md-2 col-sm-12">
                <input type="number" className="form-control form-blue quantity" defaultValue="1" min="1" onChange={(event) => {
                    props.handleInputChange(props.index, event.target.value)
                }} />
            </div> 
            <div className="cart-price col-lg-3 col-md-3 col-sm-12"><b>{formatMoney(props.productData.price)}đ</b><a href="/">Xóa</a></div>    
        </div>  
    )
}

const mapStateToProps = (state) => {
    return {
        listProduct: state.cart.listProduct
    }
}

export default connect(mapStateToProps)(Cart)