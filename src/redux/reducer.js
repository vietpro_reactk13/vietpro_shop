const initialProductState = {
    listProduct: [],
    // cart: [],
}

export function productReducer(state = initialProductState, action) {
    switch (action.type) {
        case "SET_PRODUCT_LIST":
            return {
                ...state,
                listProduct: action.payload
            }
        case "ADD_PRODUCT":
            return {
                ...state,
                listProduct: [...state.listProduct, action.payload]
            };
        case "REMOVE_PRODUCT":
            let index = state.findIndex(item => item._id === action.payload)
            return [
                ...state.slice(0, index),
                ...state.slice(index + 1, state.length)
            ]
        default:
            return state
    }
}

const initialCartState = {
    listProduct: ["5ce3c85a022e720c1aea7144", "5ce3c85a022e720c1aea7146", "5ce3c85a022e720c1aea7147", "5ce3c85a022e720c1aea714a", "5ce3c85a022e720c1aea714c", "5ce3c85a022e720c1aea7152"]
}

export function cartReducer(state = initialCartState, action) {
    switch (action.type) {
        case "ADD_PRODUCT":
            return {
                ...state,
                listProduct: [...state.listProduct, action.payload]
            }
        case "REMOVE_PRODUCT":
            let productId = action.payload
            let index = state.listProduct.findIndex(item => item === productId)

            if (index === -1) {
                return state
            }

            return {
                ...state,
                listProduct: [...state.listProduct(0, index), ...state.listProduct(index + 1, state.listProduct.length)]
            }
        case "REMOVE_ALLPRODUCT":
            return {
                ...state,
                listProduct: []
            }
        default:
            return state
    }
}

const inititalGeneralState = {
    search: ""
}

export function generalReducer(state = inititalGeneralState, action) {
    switch (action.type) {
        case "ADD_SEARCH":
            return {
                ...state,
                search: action.payload
            }
        default:
            return state
    }
}