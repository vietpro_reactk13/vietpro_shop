export const INCREASE = "INCREASE"

export function increaseProduct(product) {
    return {
        type: INCREASE,
        payload: product
    }
}