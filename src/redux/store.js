import { createStore, combineReducers } from 'redux'

import { productReducer, cartReducer, generalReducer } from './reducer'

let reducer = combineReducers({
    product: productReducer, 
    cart: cartReducer,
    general: generalReducer,
})

let store = createStore(reducer)

store.subscribe(() => {
    console.log(store.getState())
})

store.dispatch({
    type: "INCREASE",
    payload: null
})

export default store