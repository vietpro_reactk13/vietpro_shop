import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import Home from './components/Home';
import SideBar from './components/SideBar';
import Slider from './components/Slider';
import Category from './components/Category';

import store from './redux/store'
import ProductDetail from './components/ProductDetail';
import Cart from './components/Cart';
import { Search } from './components/Search';

function App() {
    return (
        <Provider store={store}>
            <Router>
                <div>
                    <Route path="/" component={Header} />
                    <div id="body">
                        <div className="container">
                            <Navbar />
                            <div className="row">
                                <div id="main" className="col-lg-8 col-md-12 col-sm-12">
                                    <Slider />
                                    <Switch>
                                        <Route exact path="/" component={Home} />
                                        <Route exact path="/category/:categoryId" component={Category} />
                                        <Route exact path="/product/:productId" component={ProductDetail} />
                                        <Route exact path="/cart" component={Cart} />
                                        <Route exact path="/search" component={Search} />
                                    </Switch>
                                </div>

                                <SideBar />
                            </div>
                        </div>
                    </div>
                    <Footer />
                </div>
            </Router>
        </Provider>

    )
}

export default App;
