import axios from 'axios'

// export const URL_BASE = "http://172.20.10.2:3001/"
export const URL_BASE = "http://localhost:3001/"

export const PRODUCT_API = URL_BASE + "api/product"
export const IMAGE_PRODUCT_BASE = URL_BASE + "images/products/"

export const api = axios.create({
    timeout: 4000,
})